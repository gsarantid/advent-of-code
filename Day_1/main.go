package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func parseInput(inputFile string) []int {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	var nums []int
	for _, entry := range strings.Fields(string(input)) {
		if num, err := strconv.Atoi(entry); err == nil {
			nums = append(nums, num)
		}
	}
	return nums
}

func findPairMatch(nums []int, target int) (int, int, error) {
	cache := make(map[int]int)

	for _, num := range nums {
		if _, ok := cache[target-num]; ok {
			return num, target - num, nil
		}
		cache[num] = num
	}
	return 0, 0, fmt.Errorf("No pair match found :(")

}

func findTripleteMatch(nums []int, target int) (int, int, int, error) {
	for index, num := range nums {
		innerNums := append(nums[0:index], nums[index:]...)
		num1, num2, error := findPairMatch(innerNums, target-num)
		if error == nil {
			return num, num1, num2, nil
		}
	}
	return 0, 0, 0, fmt.Errorf("No triplete match found :(")
}

func main() {

	nums := parseInput("input.txt")
	target := 2020

	num1, num2, error := findPairMatch(nums, target)
	if error == nil {
		fmt.Printf("Pair: (%v, %v)\n", num1, num2)
		fmt.Printf("Pair Product: %v\n", num1*num2)
	} else {
		fmt.Println(error)
	}

	num1, num2, num3, error := findTripleteMatch(nums, target)
	if error == nil {
		fmt.Printf("Triplete: (%v, %v, %v)\n", num1, num2, num3)
		fmt.Printf("Triplete Product: %v\n", num1*num2*num3)
	} else {
		fmt.Println(error)
	}
}
