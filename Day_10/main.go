package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"
)

func parseInput(inputFile string) []int {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	numbers := []int{}
	for _, line := range strings.Split(string(input), "\n") {
		number, _ := strconv.Atoi(line)
		numbers = append(numbers, number)
	}

	return numbers
}

func isValid(nums []int) bool {
	size := len(nums) - 1
	for i := 1; i < size; i++ {
		if !(nums[i]-nums[i-1] <= 3) {
			return false
		}
	}
	return true
}

func partOne(nums []int) {

	diffs := make(map[int]int)
	for i := 0; i < len(nums)-1; i++ {
		diff := nums[i+1] - nums[i]
		diffs[diff]++
	}

	fmt.Println("Part 1 =", diffs[1]*diffs[3])
}

func findNumberOfTripletes(nums []int) int {
	i := 2
	tmp := 0
	for i <= len(nums)-1 {
		if nums[i]-nums[i-2] == 2 {
			tmp++
			i += 3
		} else {
			i++
		}
	}
	return tmp
}

func partTwo(nums []int) {

	diffOne := []int{}
	for i := 1; i < len(nums)-1; i++ {
		if nums[i]-nums[i-1] < 3 && nums[i+1]-nums[i] < 3 && nums[i+1]-nums[i-1] <= 3 {
			diffOne = append(diffOne, nums[i])
		}
	}

	// Triplet corresponds to a set of three consecutive numbers e.g. 1 2 3 or 7 8 9
	numOfTripletes := findNumberOfTripletes(diffOne)

	// 2^3-1 = 7 is the number of valid combinations of a triplet if we consider its binary representation
	result := math.Pow(2, float64(len(diffOne)-numOfTripletes*3)) * math.Pow(7, float64(numOfTripletes))
	fmt.Println("Part 2 = ", result)
}

func main() {

	input := parseInput("input.txt")
	input = append(input, 0)
	sort.Ints(input)
	deviceValue := input[len(input)-1] + 3
	input = append(input, deviceValue)

	partOne(input)
	partTwo(input)
}
