package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

// Seat struct
type Seat struct {
	State string
}

func parseInput(inputFile string) [][]Seat {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n")
	rows := len(lines)
	cols := len(lines[0])
	seats := make([][]Seat, rows)

	for row, line := range strings.Split(string(input), "\n") {

		seats[row] = make([]Seat, cols)

		for column, letter := range line {
			seats[row][column] = Seat{
				State: string(letter),
			}
		}
	}
	return seats
}

func isValidSeat(row, col int, seats [][]Seat) bool {
	if row < 0 || col < 0 {
		return false
	} else if row < len(seats) && col < len(seats[0]) {
		return true
	}
	return false
}

func getAdjacentSeatsPart1(row, col int, seats [][]Seat) map[string]int {

	adjacentSeats := make(map[string]int)

	for i := row - 1; i < row+2; i++ {
		for j := col - 1; j < col+2; j++ {
			if i == row && j == col {
				continue
			}

			if isValidSeat(i, j, seats) {
				value := seats[i][j].State
				adjacentSeats[value]++
			}

		}
	}

	return adjacentSeats
}

func copySeats(seats [][]Seat) [][]Seat {
	rowsSize := len(seats)
	colsSize := len(seats[0])
	newSeats := make([][]Seat, rowsSize)

	for i := 0; i < rowsSize; i++ {
		newSeats[i] = make([]Seat, colsSize)
		for j := 0; j < colsSize; j++ {
			newSeats[i][j] = seats[i][j]
		}
	}

	return newSeats
}

func isStateStable(seats, newSeats [][]Seat) bool {
	rowsSize := len(seats)
	colsSize := len(seats[0])

	for i := 0; i < rowsSize; i++ {
		for j := 0; j < colsSize; j++ {
			if seats[i][j] != newSeats[i][j] {
				return false
			}
		}
	}
	return true
}

func prettyPrint(seats [][]Seat) {
	rowsSize := len(seats)
	colsSize := len(seats[0])

	for i := 0; i < rowsSize; i++ {
		row := ""
		for j := 0; j < colsSize; j++ {
			row = row + seats[i][j].State + " "

		}
		fmt.Println(row)
	}
}

func part1(seats [][]Seat) {

	rowsSize := len(seats)
	colsSize := len(seats[0])
	newSeats := copySeats(seats)

	occupiedSeats := 0

	for {

		for i := 0; i < rowsSize; i++ {

			for j := 0; j < colsSize; j++ {

				adjSeats := getAdjacentSeatsPart1(i, j, seats)

				if seats[i][j].State == "L" && adjSeats["#"] == 0 {
					newSeats[i][j].State = "#"
				} else if seats[i][j].State == "#" && adjSeats["#"] >= 4 {
					newSeats[i][j].State = "L"
				}

				if newSeats[i][j].State == "#" {
					occupiedSeats++
				}

			}
		}

		if isStateStable(seats, newSeats) {
			break
		}

		seats = newSeats
		newSeats = copySeats(seats)
		occupiedSeats = 0
	}

	fmt.Printf("Occupied seats are = %v\n", occupiedSeats)

}

func getAdjacentSeatsPart2(row, col int, seats [][]Seat) map[string]int {

	adjacentSeats := make(map[string]int)

	for i := row - 1; i < row+2; i++ {

		for j := col - 1; j < col+2; j++ {

			if i == row && j == col {
				continue
			}

			_i := i
			_j := j
			for isValidSeat(_i, _j, seats) && seats[_i][_j].State == "." {
				_i = _i + i - row
				_j = _j + j - col
			}

			if isValidSeat(_i, _j, seats) {
				adjacentSeats[seats[_i][_j].State]++
			} else {
				adjacentSeats["."]++
			}
		}
	}

	return adjacentSeats
}

func part2(seats [][]Seat) {

	rowsSize := len(seats)
	colsSize := len(seats[0])
	newSeats := copySeats(seats)

	occupiedSeats := 0

	for {

		for i := 0; i < rowsSize; i++ {

			for j := 0; j < colsSize; j++ {

				adjSeats := getAdjacentSeatsPart2(i, j, seats)

				if seats[i][j].State == "L" && adjSeats["#"] == 0 {
					newSeats[i][j].State = "#"
				} else if seats[i][j].State == "#" && adjSeats["#"] >= 5 {
					newSeats[i][j].State = "L"
				}

				if newSeats[i][j].State == "#" {
					occupiedSeats++
				}

			}
		}

		if isStateStable(seats, newSeats) {
			break
		}

		seats = newSeats
		newSeats = copySeats(seats)
		occupiedSeats = 0
	}

	fmt.Printf("Occupied seats are = %v\n", occupiedSeats)

}

func main() {
	seats := parseInput("input.txt")

	part1(seats)
	part2(seats)
}
