package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

// Position struct
type Position struct {
	X      int
	Y      int
	Degree int
}

// Instruction struct
type Instruction struct {
	Action string
	Value  int
}

func parseInput(inputFile string) []Instruction {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	instructions := []Instruction{}

	for _, line := range strings.Split(string(input), "\n") {

		action := string(line[0])
		value, _ := strconv.Atoi(line[1:])

		instruction := Instruction{
			Action: action,
			Value:  value,
		}

		instructions = append(instructions, instruction)

	}

	return instructions
}

func part1(instructions []Instruction) {

	position := Position{
		X:      0,
		Y:      0,
		Degree: 0,
	}

	for _, instruction := range instructions {

		switch instruction.Action {
		case "N":
			position.Y += instruction.Value
		case "E":
			position.X += instruction.Value
		case "S":
			position.Y -= instruction.Value
		case "W":
			position.X -= instruction.Value
		case "L":
			position.Degree = (position.Degree + instruction.Value) % 360
		case "R":
			position.Degree = (position.Degree - instruction.Value) % 360
			if position.Degree < 0 {
				position.Degree += 360
			}
		case "F":
			switch position.Degree {
			case 0:
				position.X += instruction.Value
			case 90:
				position.Y += instruction.Value
			case 180:
				position.X -= instruction.Value
			case 270:
				position.Y -= instruction.Value
			}
		}
	}

	fmt.Printf("X = %v -- Y = %v -- Degree = %v\n", position.X, position.Y, position.Degree)
	fmt.Printf("Distance = %v\n", math.Abs(float64(position.X))+math.Abs(float64(position.Y)))
}

func part2(instructions []Instruction) {

	position := Position{
		X:      0,
		Y:      0,
		Degree: 0,
	}

	wPosition := Position{
		X:      10,
		Y:      1,
		Degree: 0,
	}

	for _, instruction := range instructions {

		switch instruction.Action {
		case "N":
			wPosition.Y += instruction.Value
		case "E":
			wPosition.X += instruction.Value
		case "S":
			wPosition.Y -= instruction.Value
		case "W":
			wPosition.X -= instruction.Value
		case "L":
			switch instruction.Value {
			case 90:
				tmp := wPosition.X
				wPosition.X = -wPosition.Y
				wPosition.Y = tmp
			case 180:
				wPosition.X = -wPosition.X
				wPosition.Y = -wPosition.Y
			case 270:
				tmp := wPosition.X
				wPosition.X = wPosition.Y
				wPosition.Y = -tmp
			}
		case "R":
			switch instruction.Value {
			case 90:
				tmp := wPosition.X
				wPosition.X = wPosition.Y
				wPosition.Y = -tmp
			case 180:
				wPosition.X = -wPosition.X
				wPosition.Y = -wPosition.Y
			case 270:
				tmp := wPosition.X
				wPosition.X = -wPosition.Y
				wPosition.Y = tmp
			}
		case "F":
			position.X = position.X + wPosition.X*instruction.Value
			position.Y = position.Y + wPosition.Y*instruction.Value
		}
	}

	fmt.Printf("X = %v -- Y = %v\n", position.X, position.Y)
	fmt.Printf("wX = %v -- wY = %v\n", wPosition.X, wPosition.Y)
	fmt.Printf("Distance = %v\n", math.Abs(float64(position.X))+math.Abs(float64(position.Y)))
}

func main() {
	instructions := parseInput("input.txt")

	part1(instructions)
	part2(instructions)
}
