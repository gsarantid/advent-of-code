package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// Password struct
type Password struct {
	FrequencyMin int
	FrequencyMax int
	Letter       string
	Value        string
}

func parseInput(inputFile string) []Password {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	var passwords []Password
	for _, entry := range strings.Split(string(input), "\n") {
		password := strings.Fields(entry)
		frequencies := strings.Split(password[0], "-")
		minFreq, _ := strconv.Atoi(frequencies[0])
		maxFreq, _ := strconv.Atoi(frequencies[1])
		pass := Password{
			FrequencyMin: minFreq,
			FrequencyMax: maxFreq,
			Letter:       string(password[1][0]),
			Value:        password[2],
		}
		passwords = append(passwords, pass)
	}
	return passwords
}

func isValidPasswordPolicy1(password Password) bool {
	letterCount := strings.Count(password.Value, password.Letter)
	if letterCount >= password.FrequencyMin && letterCount <= password.FrequencyMax {
		return true
	}
	return false
}

func isValidPasswordPolicy2(password Password) bool {
	if (string(password.Value[password.FrequencyMin-1]) == password.Letter) != (string(password.Value[password.FrequencyMax-1]) == password.Letter) {
		return true
	}
	return false
}

func main() {
	passwords := parseInput("input.txt")

	totalValidPolicy1 := 0
	for _, password := range passwords {
		if isValidPasswordPolicy1(password) {
			totalValidPolicy1++
		}
	}
	fmt.Printf("Found %v valid passwords based on first policy\n", totalValidPolicy1)

	totalValidPolicy2 := 0
	for _, password := range passwords {
		if isValidPasswordPolicy2(password) {
			totalValidPolicy2++
		}
	}
	fmt.Printf("Found %v valid passwords based on second policy\n", totalValidPolicy2)
}
