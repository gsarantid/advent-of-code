package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

// Tree character
const Tree rune = '#'

// Slope struct
type Slope struct {
	Right int
	Down  int
}

func loadMapFile(inputFile string) []string {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	return strings.Split(string(input), "\n")
}

func calculateNumberOfTrees(Map []string, slope Slope) int {
	numOfRows := len(Map)
	columnsLimit := len(Map[0])

	numOfTrees := 0
	currentRow := 0
	currentColumn := 0
	for currentRow <= numOfRows-1 {
		if rune(Map[currentRow][currentColumn]) == Tree {
			numOfTrees++
		}
		currentColumn = (currentColumn + slope.Right) % columnsLimit
		currentRow += slope.Down
	}

	return numOfTrees
}

func main() {

	Map := loadMapFile("input.txt")

	slopeList := []Slope{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}

	productOfTrees := 1
	for _, slope := range slopeList {
		numOfTrees := calculateNumberOfTrees(Map, slope)
		fmt.Printf("%v trees have been encountered\n", numOfTrees)
		productOfTrees *= numOfTrees
	}
	fmt.Printf("Product of trees = %v\n", productOfTrees)
}
