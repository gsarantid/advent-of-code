package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func findNumberOfValidPasswords(inputFile string) {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	lines := strings.Split(string(input), "\n\n")

	validPassports := 0
	for _, line := range lines {
		line = strings.Trim(line, "\n")
		line = strings.Replace(line, "\n", " ", -1)

		mapOfFields := make(map[string]string)
		for _, field := range strings.Fields(line) {
			fieldParts := strings.Split(field, ":")
			key := fieldParts[0]
			value := fieldParts[1]
			mapOfFields[key] = value
		}

		passport := Passport{
			BirthYear:      mapOfFields["byr"],
			IssueYear:      mapOfFields["iyr"],
			ExpirationYear: mapOfFields["eyr"],
			Height:         mapOfFields["hgt"],
			HairColor:      mapOfFields["hcl"],
			EyeColor:       mapOfFields["ecl"],
			PassportID:     mapOfFields["pid"],
			CountryID:      mapOfFields["cid"],
		}

		if passport.isValid() {
			validPassports++
		}
	}
	fmt.Printf("Number of valid passports is %v\n", validPassports)
}

func main() {
	findNumberOfValidPasswords("input.txt")
}
