package main

import (
	"regexp"
	"strconv"
)

// Passport struct
type Passport struct {
	BirthYear      string
	IssueYear      string
	ExpirationYear string
	Height         string
	HairColor      string
	EyeColor       string
	PassportID     string
	CountryID      string
}

func (r *Passport) isValid() bool {
	if !(r.BirthYear != "" && r.ExpirationYear != "" &&
		r.EyeColor != "" && r.Height != "" &&
		r.HairColor != "" && r.PassportID != "" &&
		r.IssueYear != "") {
		return false
	}

	return r.isBirthYearValid() && r.isIssueYearValid() &&
		r.isExpirationYearValid() && r.isHeightValid() &&
		r.isHairColorValid() && r.isEyeColorValid() &&
		r.isPassportIDValid()
}

func (r *Passport) isBirthYearValid() bool {
	birthYearInt, _ := strconv.Atoi(r.BirthYear)
	return birthYearInt >= 1920 && birthYearInt <= 2002
}

func (r *Passport) isIssueYearValid() bool {
	issueYearInt, _ := strconv.Atoi(r.IssueYear)
	return issueYearInt >= 2010 && issueYearInt <= 2020
}

func (r *Passport) isExpirationYearValid() bool {
	expYearInt, _ := strconv.Atoi(r.ExpirationYear)
	return expYearInt >= 2020 && expYearInt <= 2030
}

func (r *Passport) isHeightValid() bool {
	re := regexp.MustCompile(`^([0-9]+)(cm|in)$`)
	matches := re.FindAllStringSubmatch(r.Height, -1)
	if matches == nil {
		return false
	}

	prefix, _ := strconv.Atoi(matches[0][1])
	suffix := matches[0][2]

	if suffix == "cm" {
		return prefix >= 150 && prefix <= 193
	}

	if suffix == "in" {
		return prefix >= 59 && prefix <= 76
	}

	return false
}

func (r *Passport) isHairColorValid() bool {
	re := regexp.MustCompile(`^#[0-9a-f]{6}$`)
	return re.MatchString(r.HairColor)
}

func (r *Passport) isEyeColorValid() bool {
	re := regexp.MustCompile(`^(amb|blu|brn|gry|grn|hzl|oth)$`)
	return re.MatchString(r.EyeColor)
}

func (r *Passport) isPassportIDValid() bool {
	re := regexp.MustCompile(`^[0-9]{9}$`)
	return re.MatchString(r.PassportID)
}
