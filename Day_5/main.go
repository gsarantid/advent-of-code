package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strings"
)

// Direction characters
const (
	Front rune = 'F'
	Back  rune = 'B'
	Left  rune = 'L'
	Right rune = 'R'
)

func parseInput(inputFile string) []string {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	return strings.Split(string(input), "\n")
}

func binarySpacePartitioning(boardingPassSlice string, lowerBound, upperBound int) int {

	for _, char := range boardingPassSlice {
		switch char {
		case Front, Left:
			upperBound = int(math.Floor((float64(upperBound) + float64(lowerBound)) / 2))
		case Back, Right:
			lowerBound = int(math.Ceil((float64(upperBound) + float64(lowerBound)) / 2))
		default:
			log.Fatal("Invalid Character --> ", string(char))
		}
	}

	if lowerBound != upperBound {
		log.Fatal("Mismatch between lower and upper bound")
	}

	return lowerBound
}

func calculateSeatID(boardingPass string) int {
	rowSlice := boardingPass[:7]
	columnSlice := boardingPass[7:]

	row := binarySpacePartitioning(rowSlice, 0, 127)
	column := binarySpacePartitioning(columnSlice, 0, 7)

	return row*8 + column
}

func main() {
	boardingPasses := parseInput("input.txt")

	minSeatID := math.MaxInt64
	maxSeatID := 0
	occupiedSeats := []int{}

	for _, boardingPass := range boardingPasses {
		seatID := calculateSeatID(boardingPass)
		occupiedSeats = append(occupiedSeats, seatID)
		maxSeatID = Max(maxSeatID, seatID)
		minSeatID = Min(minSeatID, seatID)
	}
	fmt.Printf("Min seat id --> %v\n", minSeatID)
	fmt.Printf("Max seat id --> %v\n", maxSeatID)

	allSeats := MakeRange(minSeatID, maxSeatID)
	availableSeats := SubtractSlices(allSeats, occupiedSeats)
	fmt.Printf("My seat id --> %v\n", availableSeats[0])
}
