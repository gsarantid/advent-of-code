package main

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

// MakeRange returns a sequence of numbers from min to max
func MakeRange(min, max int) []int {
	seqSlice := make([]int, max-min+1)
	for i := range seqSlice {
		seqSlice[i] = min + i
	}
	return seqSlice
}

// SubtractSlices returns the difference between two slices
func SubtractSlices(a, b []int) (diff []int) {
	m := make(map[int]bool)

	for _, item := range b {
		m[item] = true
	}

	for _, item := range a {
		if _, ok := m[item]; !ok {
			diff = append(diff, item)
		}
	}
	return
}
