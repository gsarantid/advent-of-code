package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func parseInput(inputFile string) []string {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	groups := strings.Split(string(input), "\n\n")
	return groups
}

func calculateQuestionsCountPart1(groups []string) int {
	totalQuestions := 0

	for _, group := range groups {
		uniqueAnswers := make(map[string]string)
		group = strings.Replace(group, "\n", "", -1)
		for _, char := range group {
			uniqueAnswers[string(char)] = string(char)
		}
		totalQuestions += len(uniqueAnswers)
	}

	return totalQuestions
}

func initializeCommonAnswersMap() map[string]string {
	alphabet := "abcdefghijklmnopqrstuvwxyz"
	m := make(map[string]string)
	for _, char := range alphabet {
		m[string(char)] = string(char)
	}
	return m
}

func calculateQuestionsCountPart2(groups []string) int {
	totalQuestions := 0

	for _, group := range groups {

		commonGroupAnswers := initializeCommonAnswersMap()
		lines := strings.Split(group, "\n")

		for _, line := range lines {

			uniqueAnswers := make(map[string]string)

			for _, char := range line {
				uniqueAnswers[string(char)] = string(char)
			}

			for key := range commonGroupAnswers {
				if _, ok := uniqueAnswers[key]; !ok {
					delete(commonGroupAnswers, key)
				}
			}
		}

		totalQuestions += len(commonGroupAnswers)
	}

	return totalQuestions
}

func main() {
	groups := parseInput("input.txt")

	totalQuestions := calculateQuestionsCountPart1(groups)
	fmt.Println("Total questions part 1 --> ", totalQuestions)

	totalQuestions = calculateQuestionsCountPart2(groups)
	fmt.Println("Total questions part 2 --> ", totalQuestions)
}
