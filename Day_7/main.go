package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// EMPTY string
const EMPTY = "no other"

// ShinyGold string
const ShinyGold = "shiny gold"

// BagsInfo map
type BagsInfo map[string]map[string]string

func maxNumberOfSlice(list []int) int {
	maxNum := 0
	for _, elem := range list {
		if elem > maxNum {
			maxNum = elem
		}
	}
	return maxNum
}

func checkBag(bags BagsInfo, bag string) int {
	if bag == ShinyGold {
		return 1
	} else if _, ok := bags[bag]; !ok {
		return 0
	} else {
		res := []int{}
		for key := range bags[bag] {
			res = append(res, checkBag(bags, key))
		}
		return maxNumberOfSlice(res)
	}
}

func cleanUpBagValue(bagValue string) []string {
	bagValue = strings.Replace(bagValue, "bags", "", -1)
	bagValue = strings.Replace(bagValue, "bag", "", -1)
	bagValue = strings.TrimSpace(bagValue)
	return strings.Split(bagValue, " , ")
}

func getBagsInfo(inputFile string) BagsInfo {

	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	bags := make(BagsInfo)
	for _, line := range strings.Split(string(input), "\n") {
		re := regexp.MustCompile(`^([a-z]+ [a-z]+) bags contain (.+)\.$`)
		matches := re.FindAllStringSubmatch(line, -1)
		if matches == nil {
			continue
		}

		bagKey := matches[0][1]
		bagValueList := cleanUpBagValue(matches[0][2])

		re = regexp.MustCompile(`^([0-9]+) (.+)$`)
		bags[bagKey] = make(map[string]string)

		for _, value := range bagValueList {
			if value == EMPTY {
				break
			}
			match := re.FindAllStringSubmatch(value, -1)
			if match != nil {
				bags[bagKey][match[0][2]] = match[0][1]
			}
		}
	}

	return bags
}

func calculateIndividualBags(bags BagsInfo, bagType string) int {
	counter := 0
	for key, value := range bags[bagType] {
		v, _ := strconv.Atoi(value)
		counter += v + v*calculateIndividualBags(bags, key)
	}
	return counter
}

func main() {

	bags := getBagsInfo("input.txt")

	numberOfBags := 0
	for key := range bags {
		if key != ShinyGold {
			numberOfBags += checkBag(bags, key)
		}
	}
	fmt.Println("Number of bags part 1 = ", numberOfBags)

	numberOfBags = calculateIndividualBags(bags, ShinyGold)
	fmt.Println("Number of bags part 2 = ", numberOfBags)
}
