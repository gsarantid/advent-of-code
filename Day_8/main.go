package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

// Instructions type
type Instructions map[int]map[string]interface{}

func parseInput(inputFile string) Instructions {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	instructions := make(Instructions)
	re := regexp.MustCompile(`^([a-z]+) ([+|-][0-9]+)$`)

	for index, line := range strings.Split(string(input), "\n") {

		instructions[index+1] = make(map[string]interface{})
		matches := re.FindAllStringSubmatch(line, -1)

		if matches != nil {
			instructionType := matches[0][1]
			instructionSign := matches[0][2][0]
			instructionValue, _ := strconv.Atoi(matches[0][2][1:])
			if string(instructionSign) == "-" {
				instructionValue *= -1
			}

			instructions[index+1]["operation"] = instructionType
			instructions[index+1]["value"] = instructionValue
		}
	}

	return instructions
}

func getNextInstruction(instructions Instructions, index int, acc int) (int, int) {
	operation := (instructions[index]["operation"]).(string)
	value := (instructions[index]["value"]).(int)

	switch operation {
	case "acc":
		acc += value
		index++
	case "jmp":
		index += value
	case "nop":
		index++
	}

	return index, acc
}

func getNextIndex(instructions Instructions, index int) int {
	operation := (instructions[index]["operation"]).(string)
	value := (instructions[index]["value"]).(int)

	switch operation {
	case "acc":
		index++
	case "jmp":
		index += value
	case "nop":
		index++
	}

	return index
}

func modifyInstruction(instructions *Instructions, index int) {
	operation := (*instructions)[index]["operation"]

	switch operation {
	case "acc":
	case "jmp":
		(*instructions)[index]["operation"] = "nop"
	case "nop":
		(*instructions)[index]["nop"] = "operation"
	}
}

func getCopyOfInstructions(instructions Instructions) Instructions {
	copyInstructions := make(Instructions)

	for index, instruction := range instructions {
		copyInstructions[index] = make(map[string]interface{})
		copyInstructions[index]["operation"] = instruction["operation"]
		copyInstructions[index]["value"] = instruction["value"]
	}

	return copyInstructions
}

func runPart1(instructions Instructions) {
	currentIndex := 1
	accValue := 0
	usedIndices := make(map[int]interface{})
	for {
		if _, ok := usedIndices[currentIndex]; ok {
			break
		}

		usedIndices[currentIndex] = currentIndex
		currentIndex, accValue = getNextInstruction(instructions, currentIndex, accValue)
	}

	fmt.Println("Part 1: Acc = ", accValue)
}

func executeInstructions(instructions Instructions, currentIndex int) (bool, int) {

	instructionsSize := len(instructions)
	copyInstructions := getCopyOfInstructions(instructions)
	modifyInstruction(&copyInstructions, currentIndex)
	usedIndices := make(map[int]interface{})
	index := 1
	accValue := 0

	for {

		if index > instructionsSize {
			return true, accValue
		}

		if _, ok := usedIndices[index]; ok {
			return false, accValue
		}

		usedIndices[index] = index
		index, accValue = getNextInstruction(copyInstructions, index, accValue)
	}
}

func runPart2(instructions Instructions) {
	currentIndex := 1
	accValue := 0
	terminated := false
	for {
		terminated, accValue = executeInstructions(instructions, currentIndex)
		if terminated {
			break
		}
		currentIndex = getNextIndex(instructions, currentIndex)
	}

	fmt.Println("Part 2: Acc = ", accValue)
}

func main() {
	instructions := parseInput("input.txt")

	runPart1(instructions)
	runPart2(instructions)
}
