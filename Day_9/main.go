package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

func parseInput(inputFile string) []int {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatal(err)
	}

	numbers := []int{}
	for _, line := range strings.Split(string(input), "\n") {
		number, _ := strconv.Atoi(line)
		numbers = append(numbers, number)
	}

	return numbers
}

func findPairMatch(nums []int, target int) bool {
	cache := make(map[int]int)

	for _, num := range nums {
		if _, ok := cache[target-num]; ok {
			return true
		}
		cache[num] = num
	}

	return false
}

func part1(numbers []int) int {
	preamble := 25
	numbersSize := len(numbers)
	startIndex := 0

	for startIndex < numbersSize-preamble {
		if !findPairMatch(numbers, numbers[startIndex+preamble+1]) {
			break
		}
		startIndex++
	}

	return numbers[startIndex+preamble+1]
}

func minMaxNumbersOfSlice(list []int) (int, int) {
	minNum := math.MaxInt64
	maxNum := 0
	for _, elem := range list {
		if elem < minNum {
			minNum = elem
		}
		if elem > maxNum {
			maxNum = elem
		}
	}
	return minNum, maxNum
}

func searchContiguousNumbers(numbers []int, startIndex int, target int) int {
	sum := 0
	cache := []int{}

	for _, value := range numbers[startIndex:] {
		sum += value
		cache = append(cache, value)

		if sum > target {
			return 0
		}

		if sum == target {
			min, max := minMaxNumbersOfSlice(cache)
			return min + max
		}
	}

	return 0
}

func part2(numbers []int, target int) int {
	startIndex := 0
	for startIndex < len(numbers) {
		result := searchContiguousNumbers(numbers, startIndex, target)
		if result != 0 {
			return result
		}
		startIndex++
	}
	return 0
}

func main() {

	numbers := parseInput("input.txt")

	invalidNum := part1(numbers)
	fmt.Println("Invalid num = ", invalidNum)

	encryptionWeakness := part2(numbers, invalidNum)
	if encryptionWeakness != 0 {
		fmt.Println("Part 2 encryption weakness = ", encryptionWeakness)
	} else {
		fmt.Println("Part 2 encryption weakness = no match :(")
	}
}
